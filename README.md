# Slothd-Cli
[![NPM](https://nodei.co/npm/slothd-cli.png)](https://nodei.co/npm/slothd-cli/)

# Usage

## Install
install ' slothd-cli ' with npm or yarn

```bash
$ npm i slothd-cli -g
$ yarn global add slothd-cli
```

## AddSite
add site with slothd-cli

```bash
$ slothd-cli addsite SiteName
$ cd SiteName
$ npm / yarn install
```

## AddComponent
add a component

```bash
$ slothd-cli addcomponent
? 请输入组件名 componentname
? 组件描述 component description
? 当前目录不存在components文件夹，是否创建？ Yes
√ components 创建成功 :)
√ 组件创建成功
```