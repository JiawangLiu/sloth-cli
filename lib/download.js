const download = require('download-git-repo');
const path = require('path');
const ora = require('ora')

module.exports = function (target) {
    return new Promise((resolve, reject) => {
      const url = 'bitbucket:JiawangLiu/sloth-template#master'
      const spinner = ora(`正在下载项目模板，源地址：${url}`)
      spinner.start()
      download(url, target, (err) => {
            if (err) {
                spinner.fail()
                reject(err)
            } else {
                spinner.succeed()
                resolve(target)
            }
        })
    })
}